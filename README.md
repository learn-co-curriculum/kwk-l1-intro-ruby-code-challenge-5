## Today's Code Challenge!!

You’ve been hired by Facebook to rebuild their User class, congrats! Create a
file named `user.rb` and build out the User class. Think about the attributes
and actions that an instance of the User class will need (age, name, birthday)
and actions they should be able to take (ability to post on a wall, poke, etc).

Hints:

+ Attribute accessors are your friends - you can use them in place of reader and
writer methods (like name and name=).
+ Think about how Facebook users interact and the type of attributes and actions
you will need. For instance the user might have a number of pokes that increases
after they are poked by another user.
+ Don’t forget about instance variables!
+ Here's the completed gnome lab we did previously [without attribute accessors](https://github.com/dfenjves/gnomecode/blob/master/gnome_no_accessors.rb) and [with attribute accessors for reference](https://github.com/dfenjves/gnomecode/blob/master/gnome.rb.

Challenges:

+ Can you create a way to _add_ friends to a particular instance of a user? Friends
are just other users right?  If only there was a way to store them in friends list...
+ Probably want to add unfriend functionality as well.. for when... you know...
+ Posting once one wall is fine, but a wall has a record of _every_ post you make.
Write an attribute that keeps track of all posts you've made.  Once this is
done, write a instance method that puts out _all_ of your posts, each on a new
line, to mimic your wall.
+ Now that you've got a friends list, can you access a _friend's_ wall through a
different user?
+ Can you write a method to _post_ on a friend's wall? This would mean one user
would need to a method that _adds_ a given message using someone else's methods.

<p data-visibility='hidden'>KWK-L1 Today's Code Challenge!!</p>
